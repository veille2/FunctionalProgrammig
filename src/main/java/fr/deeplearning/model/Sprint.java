package fr.deeplearning.model;

import lombok.Builder;
import lombok.Value;

import java.util.Set;

@Value
@Builder
public class Sprint {

    private int id;

    private Set<Story> stories;

    private Set<Programmer> programmers;
}
