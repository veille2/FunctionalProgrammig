package fr.deeplearning.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Language {

    private final Integer id;

    private final String name;

}



