package fr.deeplearning.model;

public enum ProfileType {
    FRONTEND, BACKEND, FULLSTACK, DEVOPS
}
