package fr.deeplearning.model;

public enum TaskType {
    FRONTEND, BACKEND, RUN, OPS
}
