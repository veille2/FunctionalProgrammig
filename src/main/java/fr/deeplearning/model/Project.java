package fr.deeplearning.model;

import lombok.Builder;
import lombok.Value;

import java.util.HashSet;
import java.util.Set;

@Value
@Builder
public class Project {

    private Integer id;

    private String name;

    private Set<Programmer> programmers = new HashSet<>();

    private Set<Story> stories = new HashSet<>();
}
