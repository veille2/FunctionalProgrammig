package fr.deeplearning.model;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Value
@Builder
public class Story {

    private Integer id;

    private final String name;

    private final TaskType type;

    private final LocalDate createdOn;

    private boolean done = false;

    private int storyPoints;

    private Set<String> tags = new HashSet<>();

    private LocalDate dueOn;
}
