package fr.deeplearning.model;


import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Value
@Builder
public class Programmer {

    private String name;

    private BigDecimal salary;

    private int age;

    private ProfileType profile;

    private Set<Story> stories;

    private Set<Language> languages = new HashSet<>();

}
