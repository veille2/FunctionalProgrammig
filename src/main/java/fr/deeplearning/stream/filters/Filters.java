package fr.deeplearning.stream.filters;

import fr.deeplearning.model.ProfileType;
import fr.deeplearning.model.Programmer;
import fr.deeplearning.model.Story;
import fr.deeplearning.model.TaskType;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

public class Filters {

    //1. With predicate
    public List<Story> getTasksByType(List<Story> stories, TaskType taskType) {
        return stories.stream()
                      .filter(isTypeTaskAs(taskType))
                      .collect(toList());
    }

    //2. With negate predicate
    public List<Story> excludeTasksOfGivenType(List<Story> stories, TaskType taskType) {
        return stories.stream()
                      .filter(isTaskNotOfType(taskType))
                      .collect(toList());
    }

    // 3. Multiple Filters
    public List<Programmer> getProgrammersByProfileAndTaskType(List<Programmer> programmers, ProfileType profileType,
                                                               TaskType taskType) {
        return programmers.stream()
                          .filter(isProgrammerProfileOfType(profileType))
                          .filter(idDoingTaskOfType(taskType))
                          .collect(toList());
    }

    //4. Chaining predicates
    public List<Programmer> getProgrammersByProfileAndTaskTypeV2(List<Programmer> programmers, ProfileType profileType,
                                                                 TaskType taskType) {
        return programmers.stream()
                          .filter(isProgrammerProfileOfType(profileType)
                                          .and(idDoingTaskOfType(taskType)))
                          .collect(toList());
    }

    //5. Combining a Collection of Predicates
    private Predicate<Programmer> idDoingTaskOfType(TaskType taskType) {
        return programmer -> programmer.getStories()
                                       .stream()
                                       .anyMatch(isTypeTaskAs(taskType));
    }

    private Predicate<Programmer> isProgrammerProfileOfType(ProfileType profileType) {
        return programmer -> Objects.equals(profileType, programmer.getProfile());
    }

    private Predicate<Story> isTypeTaskAs(TaskType taskType) {
        return task -> Objects.equals(taskType, task.getType());
    }

    private Predicate<Story> isTaskNotOfType(TaskType taskType) {
        return isTypeTaskAs(taskType).negate();
    }
}
