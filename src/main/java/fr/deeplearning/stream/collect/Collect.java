package fr.deeplearning.stream.collect;

import fr.deeplearning.model.ProfileType;
import fr.deeplearning.model.Programmer;
import fr.deeplearning.model.Sprint;
import fr.deeplearning.model.Story;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Collect {

    public Map<ProfileType, Programmer> classifyProgrammersByProfileType(List<Programmer> programmers) {
        return programmers.stream()
                          .collect(Collectors.toMap(p -> p.getProfile(), Function.identity(), (p1, p2) -> p1));
    }

    public int calculateStoriesPointForASprint(Sprint sprint) {
        return sprint.getStories()
                     .stream()
                     .collect(Collectors.summingInt(Story::getStoryPoints));
    }


}
