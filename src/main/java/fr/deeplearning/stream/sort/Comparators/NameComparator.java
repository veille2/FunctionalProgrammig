package fr.deeplearning.stream.sort.Comparators;


import java.util.Comparator;

public interface NameComparator<T> {

    Comparator<T> comparatorByName();
}
