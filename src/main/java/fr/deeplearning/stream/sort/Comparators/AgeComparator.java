package fr.deeplearning.stream.sort.Comparators;


import java.util.Comparator;

public interface AgeComparator<T> {

    Comparator<T> comparatorByAgeDesc();
}
