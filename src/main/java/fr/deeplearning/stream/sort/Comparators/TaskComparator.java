package fr.deeplearning.stream.sort.Comparators;

import fr.deeplearning.model.Story;

import java.util.Comparator;

public class TaskComparator implements NameComparator<Story> {


    @Override
    public Comparator<Story> comparatorByName() {
        return Comparator.comparing(Story::getName);
    }
}
