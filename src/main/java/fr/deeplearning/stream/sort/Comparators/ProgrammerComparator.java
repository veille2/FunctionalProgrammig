package fr.deeplearning.stream.sort.Comparators;


import fr.deeplearning.model.Programmer;

import java.util.Comparator;

public class ProgrammerComparator implements NameComparator<Programmer>, AgeComparator<Programmer> {


    @Override
    public Comparator<Programmer> comparatorByName() {
        return Comparator.comparing(Programmer::getName);
    }

    @Override
    public Comparator<Programmer> comparatorByAgeDesc() {
        return Comparator.comparing(Programmer::getAge).reversed();
    }
}
