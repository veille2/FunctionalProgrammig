package fr.deeplearning.stream.sort;


import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class SortClass {

    public <T> List<T> sortList(List<T> list, Comparator<T> comparator) {
        return list.stream()
                   .sorted(comparator)
                   .collect(toList());
    }
}
