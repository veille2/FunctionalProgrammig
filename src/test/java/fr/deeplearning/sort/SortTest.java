package fr.deeplearning.sort;

import fr.deeplearning.stream.sort.Comparators.ProgrammerComparator;
import fr.deeplearning.stream.sort.SortClass;
import fr.deeplearning.stream.sort.Comparators.TaskComparator;
import org.assertj.core.api.Assertions;

import org.junit.Before;
import org.junit.Test;


import java.util.Arrays;

import static fr.deeplearning.sort.Fixtures.*;

public class SortTest {

    private SortClass sortClass;

    private ProgrammerComparator programmerComparator;

    private TaskComparator taskComparator;

    @Before
    public void init() {

        sortClass = new SortClass();
        programmerComparator = new ProgrammerComparator();
        taskComparator = new TaskComparator();
    }

    @Test
    public void testSortProgrammersByName() {

        //Given
        var programmers = Arrays.asList(JAVA_PROGRAMMER, GO_PROGRAMMER, REACT_PROGRAMMER);
        var expectedResult = Arrays.asList(GO_PROGRAMMER, JAVA_PROGRAMMER, REACT_PROGRAMMER);

        //When
        var result = sortClass.sortList(programmers, programmerComparator.comparatorByName());

        //Then
        Assertions.assertThat(expectedResult).hasSameElementsAs(result);
    }

    @Test
    public void testSortTasksByName() {

        //Given
        var tasks = Arrays.asList(LOGIN_ENDPOINT, FIX_PROJECTS_WITHOUT_NAME, LOGIN_PAGE);
        var expectedResult = Arrays.asList(LOGIN_ENDPOINT, LOGIN_PAGE, FIX_PROJECTS_WITHOUT_NAME);

        //When
        var result = sortClass.sortList(tasks, taskComparator.comparatorByName());

        //Then
        Assertions.assertThat(expectedResult).hasSameElementsAs(result);
    }

    @Test
    public void testSortProgrammersByAgeDesc() {

        //Given
        var programmers = Arrays.asList(REACT_PROGRAMMER, JAVA_PROGRAMMER, GO_PROGRAMMER);
        var expectedResult = Arrays.asList(JAVA_PROGRAMMER, GO_PROGRAMMER, REACT_PROGRAMMER);

        //When
        var result = sortClass.sortList(programmers, programmerComparator.comparatorByAgeDesc());

        //Then
        Assertions.assertThat(expectedResult).hasSameElementsAs(result);
    }
}
