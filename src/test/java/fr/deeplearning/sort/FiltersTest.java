package fr.deeplearning.sort;

import fr.deeplearning.model.ProfileType;
import fr.deeplearning.model.TaskType;
import fr.deeplearning.stream.filters.Filters;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static fr.deeplearning.sort.Fixtures.*;

public class FiltersTest {

    private Filters filters;

    @Before
    public void init() {

        filters = new Filters();
    }

    @Test
    public void testGetTasksByType() {

        //Given
        var tasks = Arrays.asList(LOGIN_ENDPOINT, CI_CD, LOGIN_PAGE);
        var expectedResult = Arrays.asList(CI_CD);

        //When
        var result = filters.getTasksByType(tasks, TaskType.OPS);

        //Then
        Assertions.assertThat(expectedResult).hasSameElementsAs(result);
    }

    @Test
    public void testExcludeTasks() {

        //Given
        var tasks = Arrays.asList(LOGIN_ENDPOINT, CI_CD, LOGIN_PAGE);
        var expectedResult = Arrays.asList(LOGIN_ENDPOINT, CI_CD);

        //When
        var result = filters.excludeTasksOfGivenType(tasks, TaskType.FRONTEND);

        //Then
        Assertions.assertThat(expectedResult).hasSameElementsAs(result);
    }

    @Test
    public void testGetProgrammersByProfileAndTaskType() {

        //Given
        var programmers = Arrays.asList(JAVA_PROGRAMMER, GO_PROGRAMMER, REACT_PROGRAMMER, SPRING_PROGRAMMER);
        var expectedResult = Arrays.asList(JAVA_PROGRAMMER, SPRING_PROGRAMMER);
        //When
        var result = filters.getProgrammersByProfileAndTaskType(programmers, ProfileType.BACKEND, TaskType.OPS);

        //Then
        Assertions.assertThat(expectedResult).hasSameElementsAs(result);
    }

}
