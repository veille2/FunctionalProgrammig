package fr.deeplearning.sort;

import fr.deeplearning.model.ProfileType;
import fr.deeplearning.model.Programmer;
import fr.deeplearning.model.Story;
import fr.deeplearning.model.TaskType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

public class Fixtures {

    public static Story LOGIN_PAGE = Story.builder()
            .name("create login page")
            .createdOn(LocalDate.of(2019, 8, 8))
            .type(TaskType.FRONTEND)
            .build();


    public static Story LOGIN_ENDPOINT= Story.builder()
            .name("create login endpoint")
            .createdOn(LocalDate.of(2019, 8, 14))
            .type(TaskType.BACKEND)
            .build();


    public static Story FIX_PROJECTS_WITHOUT_NAME= Story.builder()
            .name("fix the problem of projects that haven't name")
            .createdOn(LocalDate.of(2019, 7, 8))
            .type(TaskType.RUN)
            .build();

    public static Story CI_CD = Story.builder()
            .name("Continuous Integration and Continuous Delivery")
            .createdOn(LocalDate.of(2019, 9, 02))
            .type(TaskType.OPS)
            .build();

    public static Programmer JAVA_PROGRAMMER = Programmer.builder()
            .name("Lydia")
            .age(29)
            .profile(ProfileType.BACKEND)
            .salary(BigDecimal.valueOf(4500))
            .stories(Set.of(CI_CD, FIX_PROJECTS_WITHOUT_NAME))
            .build();

    public static Programmer SPRING_PROGRAMMER = Programmer.builder()
            .name("Jonathan")
            .age(34)
            .profile(ProfileType.BACKEND)
            .salary(BigDecimal.valueOf(5500))
            .stories(Set.of(CI_CD, LOGIN_PAGE))
            .build();

    public static Programmer REACT_PROGRAMMER = Programmer.builder()
            .name("Safir")
            .age(27)
            .salary(BigDecimal.valueOf(3500))
            .build();

    public static Programmer GO_PROGRAMMER = Programmer.builder()
            .name("Hamza")
            .age(28)
            .salary(BigDecimal.valueOf(4000))
            .build();
}
